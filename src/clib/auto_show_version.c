#include <stdio.h>
void auto_show_version(FILE *fp) {
   fprintf (fp,"\n");
   fprintf (fp,"The Grackle Version 3.2.dev2\n");
   fprintf (fp,"Git Branch   master\n");
   fprintf (fp,"Git Revision d6f78847a5f3690d443009c8cdefe37cd14f3b36\n");
   fprintf (fp,"\n");
}
